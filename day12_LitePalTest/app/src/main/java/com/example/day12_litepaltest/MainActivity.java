package com.example.day12_litepaltest;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import org.litepal.LitePal;
import org.litepal.crud.LitePalSupport;

import java.util.List;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = "MainActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button button_create = findViewById(R.id.create_database);
        button_create.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LitePal.getDatabase();
            }
        });

        Button button_insert = findViewById(R.id.insert);
        button_insert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Book book = new Book();
                book.setName("老人与海");
                book.setAuthor("海明威");
                book.setPages(456);
                book.setPress("呼吸出版社");
                book.setPrice(18.88);
                book.save();
            }
        });

        Button button_update = findViewById(R.id.update);
        button_update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Book book = new Book();
                book.setPrice(12.00);
                book.setPress("大头出版社");
                book.updateAll("name = ? and author = ?", "老人与海", "海明威");
            }
        });

        Button button_delete = findViewById(R.id.delete);
        button_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LitePal.deleteAll(Book.class, "price < ?", "15");
            }
        });

        Button button_query = findViewById(R.id.quary);
        button_query.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                List<Book> books = LitePal.findAll(Book.class);
                for (Book book : books){
                    Log.d(TAG, "--------------------");
                    Log.d(TAG, "书名："+book.getName());
                    Log.d(TAG, "作者："+book.getAuthor());
                    Log.d(TAG, "出版社："+book.getPress());
                    Log.d(TAG, "价格："+book.getPrice());
                    Log.d(TAG, "页数："+book.getPages());
                    Log.d(TAG, "--------------------");
                }
            }
        });
    }
}
