package com.example.day08_broadcasttest;

import androidx.appcompat.app.AppCompatActivity;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkCapabilities;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;


public class MainActivity extends AppCompatActivity {
    private NetWorkChangeReceiver netWorkChangeReceiver;

    public MainActivity() {
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(netWorkChangeReceiver);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.net.conn.CONNECTIVITY_CHANGE");
        netWorkChangeReceiver = new NetWorkChangeReceiver();
        registerReceiver(netWorkChangeReceiver, intentFilter);

        Button button = (Button) findViewById(R.id.button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setComponent(new ComponentName(MainActivity.this.getPackageName(), "com.example.day08_broadcasttest.MyBroadcastReceiver"));
                sendBroadcast(intent);
            }
        });
    }

    private class NetWorkChangeReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
                if (connectivityManager != null) {
                    Network activeNetwork = connectivityManager.getActiveNetwork();
                    NetworkCapabilities networkCapabilities = connectivityManager.getNetworkCapabilities(activeNetwork);
                    if (networkCapabilities != null && networkCapabilities.hasCapability(NetworkCapabilities.NET_CAPABILITY_INTERNET)) {
                        Toast.makeText(context,"已连接到网络", Toast.LENGTH_LONG).show();
                        if(networkCapabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI)){
                            Toast.makeText(context,"wifi连接网络",Toast.LENGTH_SHORT).show();
                        }else{
                            Toast.makeText(context,"数据流量连接网络",Toast.LENGTH_SHORT).show();
                        }
                    }else{
                        Toast.makeText(context,"未连接到网络",Toast.LENGTH_SHORT).show();
                    }
                }else{
                    Toast.makeText(context,"没有可用网络", Toast.LENGTH_SHORT).show();
                }
            }
        }
    }
}
