package com.example.day22_skills;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

public class People implements Parcelable {
    private String name;
    private int age;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeInt(age);
    }

    public static final Parcelable.Creator<People> CREATOR = new Parcelable.Creator<People>(){
        @Override
        public People createFromParcel(Parcel source) {
            People people = new People();
            people.name = source.readString();
            people.age = source.readInt();
            return people;
        }

        @Override
        public People[] newArray(int size) {
            return new People[size];
        }
    };
}
