package com.example.day14_notificationtest;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Vibrator;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.io.File;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button button_sendNotice = findViewById(R.id.send_notice);
        button_sendNotice.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.send_notice:
                Intent intent = new Intent(this, NotificationActivity.class);
                PendingIntent pi = PendingIntent.getActivity(this, 0, intent, 0);
                NotificationManager manager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
                NotificationCompat.Builder builder = new NotificationCompat.Builder(this, "unique");
                builder.setContentTitle("这是标题")
                        .setPriority(NotificationCompat.PRIORITY_MAX)
                        .setAutoCancel(true)
                        .setContentText("这是文本")
                        .setContentInfo("这是内容")
                        .setSubText("这是小字")
                        .setTicker("滚动消息......")
                        .setVibrate(new long[]{ 0, 1000, 1000, 1000 })
                        .setWhen(System.currentTimeMillis()) // 出现时间
                        .setSmallIcon(R.mipmap.ic_launcher) //小图标
                        .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher))  // 大图标
                        .setContentIntent(pi); //设置响应
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                    NotificationChannel notificationChannel = new NotificationChannel("unique", "TEST", NotificationManager.IMPORTANCE_HIGH);
                    notificationChannel.setVibrationPattern(new long[]{ 0, 1000, 1000, 1000 });
                    notificationChannel.enableVibration(true);
                    manager.createNotificationChannel(notificationChannel);
                }
                Notification notification = builder.build();
                manager.notify(1, notification);
                break;
            default:
                break;
        }
    }
}
