package com.example.day09_mybroadcastreceiver2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.IntentFilter;
import android.os.Bundle;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
//    设置为静态可以解决重复注册的问题
    static AnotherBroadCastReceiver anotherBroadCastReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (anotherBroadCastReceiver == null) {
            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction("MyBroadcast");
            // setPriority()设置权限大小
            intentFilter.setPriority(1000);
            anotherBroadCastReceiver = new AnotherBroadCastReceiver();
            registerReceiver(anotherBroadCastReceiver, intentFilter);
            Toast.makeText(this, "2 完成注册", Toast.LENGTH_LONG).show();
        }
    }
}
