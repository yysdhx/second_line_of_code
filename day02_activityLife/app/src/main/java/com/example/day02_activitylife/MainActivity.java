package com.example.day02_activitylife;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity";

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.d(TAG, "onRestart: 停止态变为运行态");
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState, @NonNull PersistableBundle outPersistentState) {
        super.onSaveInstanceState(outState, outPersistentState);
        String tempData = "这里是你刚刚输入的数据";
        outState.putString("key", tempData);
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy: 活动销毁前");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d(TAG, "onStop: 活动完全不可见");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d(TAG, "onPause: 准备启动或恢复另一个活动");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "onResume: 准备交互");
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d(TAG, "onStart: 不可见变为可见");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (savedInstanceState!=null){
            String tempData = savedInstanceState.getString("key");
            Log.d(TAG, tempData);
        }

        Log.d(TAG, "onCreate: 第一次被创建");
        Button startNormalActivity = (Button) findViewById(R.id.start_normal_activity);
        Button startDialogActivity = (Button) findViewById(R.id.start_dialog_activity);
        startNormalActivity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, NormalActivity.class);
                startActivity(intent);
            }
        });
        startDialogActivity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent  = new Intent(MainActivity.this, DialogActivity.class);
                startActivity(intent);
            }
        });

    }
}
