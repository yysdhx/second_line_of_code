package com.example.day06_fragment;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

public class RightFragment extends Fragment {
    private static final String TAG = "RightFragment";

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "onCreate: 活动第一次被创建");
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        Log.d(TAG, "onAttach: 与活动建立关联");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy: 活动被销毁");
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        Log.d(TAG, "onDestroyView: 碎片的布局被销毁");
    }

    @Override
    public void onDetach() {
        super.onDetach();
        Log.d(TAG, "onDetach: 碎片和活动已解除关联");
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.d(TAG, "onStop: 活动完全不可见");
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d(TAG, "onPause: 另一个活动准备调用");
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d(TAG, "onResume: 活动就绪，准备交互！");
    }

    @Override
    public void onStart() {
        super.onStart();
        Log.d(TAG, "onStart: 活动由不可见变为可见");
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Log.d(TAG, "onActivityCreated: 活动创建完毕");
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Log.d(TAG, "onCreateView: 碎片加载布局");
        return inflater.inflate(R.layout.right_fragment, container, false);
    }
}
