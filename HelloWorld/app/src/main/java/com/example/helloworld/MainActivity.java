package com.example.helloworld;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;


public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
    }

    // 加载`toolbar.xml`菜单
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.toolbar, menu);
        return true;
    }

    // 处理按钮点击事件
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.cherry:
                Toast.makeText(this, "樱桃🍒", Toast.LENGTH_SHORT).show();
                break;
            case R.id.mongo:
                Toast.makeText(this, "芒果🍋", Toast.LENGTH_SHORT).show();
                break;
            case R.id.grape:
                Toast.makeText(this, "葡萄🍇", Toast.LENGTH_SHORT).show();
                break;
            default:
                break;
        }
        return true;
    }
}