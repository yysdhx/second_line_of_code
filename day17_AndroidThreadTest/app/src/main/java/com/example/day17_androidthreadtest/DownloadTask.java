package com.example.day17_androidthreadtest;


import android.os.AsyncTask;
import android.widget.Toast;


// 代码用来举例
public class DownloadTask extends AsyncTask<Void, Integer, Boolean> {
    @Override
    protected void onPreExecute() {
        progressDialog.show(); // 显示进度条
    }

    @Override
    protected Boolean doInBackground(Void... voids) {
        try {
            while (true){
                int donloadPercent = download(); // 虚构的方法，返回下载进度
                publishProgress(donloadPercent);
                if(donloadPercent>=100){
                    break;
                }
            }
        }catch (Exception e){
            return false;
        }
        return true;
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        // 更新下载进度
        progressDialog.setMessage("Downloaded " + values[0] + "%");
    }

    @Override
    protected void onPostExecute(Boolean aBoolean) {
        progressDialog.dismiss(); // 关闭进度条
        if (aBoolean){
            Toast.makeText(context, "下载成功", Toast.LENGTH_SHORT).show();
        }else {
            Toast.makeText(context, "下载失败", Toast.LENGTH_SHORT).show();
        }
    }
}
