package com.example.day16_networktest;

public interface HttpCallbackListener {
    void onFinish(String response);

    void onError(Exception e);
}
