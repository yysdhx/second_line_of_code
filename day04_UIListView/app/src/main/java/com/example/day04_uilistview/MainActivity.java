package com.example.day04_uilistview;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private List<Fruit> fruitList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initFruits();
        FruitAdapter adapter = new FruitAdapter(MainActivity.this, R.layout.fruit_item, fruitList);
        ListView listView = findViewById(R.id.list_view);
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Fruit fruit = fruitList.get(position);
                Toast.makeText(MainActivity.this, "你点击了 "+fruit.getName(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void initFruits(){
        for (int i = 0; i < 2; i++){
            Fruit apple = new Fruit("苹果", R.drawable.apple);
            fruitList.add(apple);
            Fruit banana = new Fruit("香蕉", R.drawable.banana);
            fruitList.add(banana);
            Fruit orange = new Fruit("橙子", R.drawable.orange);
            fruitList.add(orange);
            Fruit watermelon = new Fruit("西瓜", R.drawable.watermelon);
            fruitList.add(watermelon);
            Fruit pear = new Fruit("梨", R.drawable.pear);
            fruitList.add(pear);
            Fruit grape = new Fruit("葡萄", R.drawable.grape);
            fruitList.add(grape);
            Fruit pineapple = new Fruit("菠萝", R.drawable.pineapple);
            fruitList.add(pineapple);
            Fruit strawberry = new Fruit("草莓", R.drawable.strawberry);
            fruitList.add(strawberry);
            Fruit cherry = new Fruit("樱桃", R.drawable.cherry);
            fruitList.add(cherry);
            Fruit mango = new Fruit("芒果", R.drawable.mango);
            fruitList.add(mango);
        }
    }
}
