package com.example.day12_databasetest;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private MyDatabaseHelper dbHelper;
    private static final String TAG = "MainActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        dbHelper = new MyDatabaseHelper(this, "BookStore.db", null, 2);
        Button createDatabase = findViewById(R.id.create_database);
        Button button_insert = findViewById(R.id.insert);
        createDatabase.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dbHelper.getWritableDatabase();
            }
        });
        button_insert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SQLiteDatabase db = dbHelper.getWritableDatabase();
                ContentValues values = new ContentValues();
                // 第一条数据
                values.put("name", "老人与海");
                values.put("author", "海明威");
                values.put("pages", 456);
                values.put("price", 18.88);
                db.insert("Book", null, values);
                values.clear();
                // 第二条数据
                values.put("name", "绿野仙踪");
                values.put("author", "莱曼·弗兰克·鲍姆");
                values.put("pages", 345);
                values.put("price", 16.66);
                db.insert("Book", null, values);
            }
        });
        Button button_update = findViewById(R.id.update);
        button_update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SQLiteDatabase db = dbHelper.getReadableDatabase();
                ContentValues values = new ContentValues();
                values.put("price", 10.99);
                db.update("Book",values, "name = ?",new String[]{"老人与海"});
            }
        });
        Button button_delete = findViewById(R.id.delete);
        button_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SQLiteDatabase db = dbHelper.getReadableDatabase();
                db.delete("Book", "pages > ?", new String[]{"400"});
            }
        });
        Button button_quary = findViewById(R.id.quary);
        button_quary.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SQLiteDatabase db = dbHelper.getReadableDatabase();
                // 查询表中所有的数据
                Cursor cursor = db.query("Book",null, null, null, null, null, null);
                if (cursor.moveToFirst()){
                    do{
                        String name = cursor.getString(cursor.getColumnIndex("name"));
                        String author = cursor.getString(cursor.getColumnIndex("author"));
                        int pages = cursor.getInt(cursor.getColumnIndex("pages"));
                        double price = cursor.getDouble(cursor.getColumnIndex("price"));
                        Log.d(TAG, "--------------------");
                        Log.d(TAG, "书名："+name);
                        Log.d(TAG, "作者："+author);
                        Log.d(TAG, "页数："+pages);
                        Log.d(TAG, "价格："+price);
                        Log.d(TAG, "--------------------");
                    }while (cursor.moveToNext());
                }
                cursor.close();
            }
        });
    }
}
